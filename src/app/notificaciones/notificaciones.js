/**
 *
 * Notificaciones
 *
 */
import "./notificaciones.less";
import templateHtml from "./notificaciones.tpl.html";

import $ from 'jquery';
import localforage from 'localforage';
import Tool from '../utils/tool';
import Store from '../store';


export default {
    init() {
        this.render();
    },
    async render() {
        var Notificaciones = await Store.Notifications({
            Type: "Unread"
        });

        var renderTpl = Tool.renderTpl(templateHtml, {
            Notificaciones: Notificaciones.Data
        });

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    handleEvents() {
        $(".btnSetPopUpRead").on("click", function() {
            var $btn = $(this);
            var NotificationId = $btn.data("id");

            Store.SetPopUpRead(NotificationId).then(function(r) {
                console.log(r);
                var $notification = $("#notificacion-" + NotificationId);

                $notification.addClass('remove');
                setTimeout(function() {
                    $notification.remove();
                }, 600);
            });
        });
    }
}