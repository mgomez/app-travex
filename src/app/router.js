/**
 *
 * ROUTER
 *
 */

import $ from 'jquery';
import localforage from 'localforage';
import Tool from './utils/tool';
import Store from './store';
//layout
import layout from "./shared/layout.tpl.html";
import layoutLogin from "./shared/layoutLogin.tpl.html";
import offlineTpl from "./shared/offline.tpl.html";
//views
import login from "./login/login";
import main from "./main/main";
import saldo from "./saldo/saldo";
import comprobar from "./comprobar/comprobar";
import porFacturar from "./porFacturar/porFacturar";
import presupuestoAlta from "./presupuestoAlta/presupuestoAlta";
import express from "./express/express";
import configuracion from "./configuracion/configuracion";
import autorizarPresupuesto from "./autorizarPresupuesto/autorizarPresupuesto";
import estatus from "./estatus/estatus";
import agregarTarjeta from "./agregarTarjeta/agregarTarjeta";
import notificaciones from "./notificaciones/notificaciones";




export default {
    async View(page, _default) {
        var _this = this;
        var renderTpl = "";
        var User = await Store.GetUser();

        localforage.setItem("lastView", {
            page: page,
            layout: layout
        });
        //valido que este conectado a una red
        if (!window.Connection) {
            renderTpl = Tool.renderTpl(layoutLogin);

            $("#app").html(renderTpl);
            $("#renderBody").html(offlineTpl);
            return;
        }

        //cargo el layout
        renderTpl = (!_default) ? layout : layoutLogin;

        renderTpl = Tool.renderTpl(renderTpl, User);

        $("#app").html(renderTpl);

        _this.setActiveMenu(page);

        switch (page) {
            case 'login':
                login.init(page);
                break;
            case 'main':
                main.init(User);
                break;
            case 'saldo':
                saldo.init(page);
                break;
            case 'comprobar':
                comprobar.init(page);
                break;
            case 'porFacturar':
                porFacturar.init(page);
                break;
            case 'presupuestoAlta':
                presupuestoAlta.init(page);
                break;
            case 'agregarTarjeta':
                agregarTarjeta.init(page);
                _this.setActiveMenu("saldo");
                break;
            case 'configuracion':
                configuracion.init(page);
                break;
            case 'autorizarPresupuesto':
                autorizarPresupuesto.init(page);
                break;
            case 'estatus':
                estatus.init(page);
                break;
            case 'express':
                express.init(page);
                break;
            case 'notificaciones':
                notificaciones.init(page);
                break;


            default:
                renderTpl = Tool.renderTpl(layoutLogin);
                $("#app").html(renderTpl);
                login.init(page);
                break;
        }

        if (!_default) {
            setTimeout(function() {
                $("#btn-touchSidewipe").on("click", function() {
                    _this.setNotifications();
                    window.touchSideSwipe.tssOpen();
                });
            }, 800);
        }
    },
    setActiveMenu(page) {
        $(".menu .menu-item").removeClass('active');
        $("[data-view=" + page + "]").addClass('active');
    },
    async setNotifications() {
        var Notificaciones = await Store.Notifications({
            Type: "Unread",
            background: true
        });

        Notificaciones = Notificaciones.Data.length;

        var klass = Notificaciones > 0 ? "active" : "";

        $("#notificacionesPendientes").html(Notificaciones);

        $("#notificacionesPendientes")[0].className = klass;
    }
};