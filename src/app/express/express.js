/**
 *
 * Presupuesto Express
 *
 */

import "./express.less";
import templateHtml from './express.tpl.html';
import "pretty-checkbox/dist/pretty-checkbox.min.css";
import $ from 'jquery';
import validate from 'jquery-validation';
import localforage from 'localforage';
import Tool from '../utils/tool';
import Store from '../store';


export default {
    async init() {
        //valida que el usuario tenga acceso...
        var Pass = await Store.Pass("BudgetExpress");
        var HasPermit = Pass.Data.HasPermit;

        if (HasPermit === "0") {
            alert("El usuario no tiene permisos para solicitar un presupuesto exprés.\nPonte en contacto con el administrador.");
            app.View("main");
            return;
        }
        this.render();
    },
    async render() {
        var Empleados = await Store.Users({
            Type: "BudgetExpress"
        });

        var renderTpl = Tool.renderTpl(templateHtml, {
            Empleados: Empleados.Data
        });

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    handleEvents() {

        $("#ck-reset").on("change", function() {
            var $ck = $(this);

            if ($ck.is(":checked")) {
                alert("El saldo de la tarjeta se colocará en $0.00 previo a la dispersión.");
            }
        });

        $("#frm-express").on("submit", function() {
                var $frm = $(this);
                var data = $frm.serializeObject();

                console.log(data);

                if ($frm.valid()) {
                    app.loading(true);
                    Store.Express(data).then(function(r) {
                        app.loading(false);
                        if (!r.Success) {
                            alert(r.Message);
                        } else {
                            alert("Se realizó correctamente la operación.\nPrespuesto " + r.Data.Value);
                            app.View("main");
                        }
                    });
                }

                return false;
            })
            .validate({
                'messages': {
                    'EmployeeId': {
                        required: 'El empleado es necesario.',
                    },
                    'Amount': {
                        required: 'Debes ingresar un importe.',
                        min: "Ingresar un monto mayor a 0."
                    },
                    'Comment': {
                        required: 'Debes de ingresar un objetivo.'
                    }
                }
            });
    }
}