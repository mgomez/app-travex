/**
 *
 * _empleados
 *
 */
import "./_empleados.less";
import templateHtml from "./_empleados.tpl.html";

import $ from 'jquery';
import Jets from 'jets';
import Tool from '../../utils/tool';
import Store from '../../store';

export default {
    async render(Type) {
        var empleados = await Store.Users({
            Type: Type
        });

        var renderTpl = Tool.renderTpl(templateHtml, {
            Empleados: empleados.Data
        });

        return renderTpl;
    },
    handleEvents() {
        $("style").remove();

        var jets = new Jets({
            searchTag: '#jetsSearch',
            contentTag: '#empleadosContainer'
        });
    },
    empleadoDefault() {
        var empleadoDefault = $(".empleado").length === 1;
        if (empleadoDefault) {
            $(".empleado:first").trigger("click");
        }
    }
}