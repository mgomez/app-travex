/**
 *
 * Autorizar Presupuesto
 *
 */
import "./estatus.less";
import templateHtml from "./estatus.tpl.html";
import estatusPresupuesto from "./_estatusPresupuesto.tpl.html";

import $ from 'jquery';
import localforage from 'localforage';
import Tool from '../utils/tool';
import Store from '../store';


export default {
    init() {
        this.render();
    },
    render() {
        var renderTpl = Tool.renderTpl(templateHtml);

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    async handleEvents() {
        var _this = this;
        $("#frm-getBudget").on("submit", function() {
            var $frm = $(this);
            var data = $frm.serializeObject();

            if (!data.BudgetId) {
                alert("Ingrese un presupuesto.");
            } else {
                Store.GetBudgets(data).then(function(r) {
                    console.log(r.Data);
                    var renderTpl = Tool.renderTpl(estatusPresupuesto, {
                        Prespuesto: r.Data
                    });
                    $("#estatusContainer").html(renderTpl);
                    $('[data-toggle="popover"]').popover({
                        placement: 'top'
                    });
                });
            }
            return false;
        });
    }
}