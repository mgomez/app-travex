/**
 *
 * COMPROBAR
 *
 */
import "./porFacturar.less";
import templateHtml from "./porFacturar.tpl.html";

import $ from 'jquery';
import localforage from 'localforage';
import Enumerable from 'linq';
import Jets from 'jets';
import moment from 'moment';
import Tool from '../utils/tool';
import Store from '../store';
//Parciales
import optionsTpl from "../shared/options.tpl.html";
import _empleados from "../shared/_empleados/_empleados";
import porFacturarTransaccionesTpl from "./porFacturar-transacciones.tpl.html";
import porFacturarCollapseTpl from "./porFacturar-collapse.tpl.html";
import porFacturarDetalleTpl from "./porFacturar-detalle.tpl.html";

function periodoValido(data) {
    var fechaInicio = moment(data.fechaInicio);
    var fechaFin = moment(data.fechaFin);

    if (fechaFin < fechaInicio) {
        return false;
    }

    return true;
}

export default {
    Data: {},
    init() {
        this.Data.Employee = {};
        this.render();
    },
    async render() {
        var empleados = await _empleados.render("Expenses");

        var renderTpl = Tool.renderTpl(templateHtml, {
            Partial: empleados
        });

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    handleEvents() {
        var _this = this;
        //inicializa la funcionalidad de la vista de empleados...
        _empleados.handleEvents();

        $(".empleado").on("click", async function() {
            var $this = $(this);
            var EmployeeId = $this.data("id");
            var FullName = $this.data("name");

            _this.Data.Employee.EmployeeId = EmployeeId;
            _this.Data.Employee.FullName = FullName;

            var Transacciones = await Store.Transactions({
                EmployeeId: EmployeeId,
                StartDate: moment().add(-15, "days").format("YYYY-MM-DD"),
                EndDate: moment().format("YYYY-MM-DD")
            });

            Transacciones = Enumerable.from(Transacciones.Data)
                .where("$.Balance > 0")
                .toArray();

            _this.Data.Transacciones = Transacciones;
            _this.Data.mensajeFiltros = "*transacciones de los ultimos 15 días (10 max)";
            //Cargo la vista de transacciones...
            _this.renderPartial(porFacturarTransaccionesTpl, {
                Transacciones: Transacciones
            }, _this.handleEventsTransacciones);
        });

        _empleados.empleadoDefault();
    },
    handleEventsTransacciones(ctx) {
        moment.locale("es");
        var mensajeFiltros = ctx.Data.mensajeFiltros || "*transacciones de los ultimos 15 días (10 max)";

        $("#mensajeFiltros").html(mensajeFiltros);
        //Buscardor de transacciones
        $("style").remove();
        var jets = new Jets({
            searchTag: '#jetsSearch',
            contentTag: '#transaccionesContainer'
        });
        //boton de actualizar
        $("#btnUpdate").on("click", function() {
            ctx.goToTransactions({
                refresh: true
            });
        });
        //Filtros
        $("#modalFiltros").on("show.bs.modal", function() {
            var fechaInicio = moment().add(-15, 'days').format("YYYY-MM-DD");
            var fechaFin = moment().format("YYYY-MM-DD");

            $("#fechaInicio").val(fechaInicio);
            $("#fechaFin").val(fechaFin);
        });

        $(".dropdown-item").on("click", function() {
            var option = $(this).data("value");

            var fechaInicio = "";
            var fechaFin = "";

            switch (option) {
                case 1:
                default:
                    fechaInicio = moment().format("YYYY-MM-DD");
                    fechaFin = fechaInicio;
                    break;
                case 2:
                    fechaInicio = moment().add(-7, "days").startOf("month").format("YYYY-MM-DD");
                    fechaFin = moment().format("YYYY-MM-DD");
                    break;
                case 3:
                    fechaInicio = moment().startOf("month").format("YYYY-MM-DD");
                    fechaFin = moment().endOf("month").format("YYYY-MM-DD");
                    break;
                case 4:
                    fechaInicio = moment().add(-90, "days").format("YYYY-MM-DD");
                    fechaFin = moment().format("YYYY-MM-DD");
                    break;
            }

            $("#fechaInicio").val(fechaInicio);
            $("#fechaFin").val(fechaFin);
        });

        $("#frmFiltros").on("submit", function() {
            var $frm = $(this);
            var data = $frm.serializeObject();

            console.log(data);

            if (periodoValido(data)) {
                $("#modalFiltros").modal("hide");
                var formatDate = "ddd DD MMMM";
                mensajeFiltros = "*Ultimas " + data.Limit + " transacciones.<br>" + moment(data.fechaInicio).format(formatDate) + " - " + moment(data.fechaFin).format(formatDate);

                ctx.goToTransactions({
                    refresh: true,
                    data: data,
                    mensajeFiltros: mensajeFiltros
                });
            } else {
                alert("La fecha fin debe ser mayor a la fecha de inicio.");
            }
            return false;
        });

        $("#btnCantidadComprobaciones button").on("click", function() {
            var $btn = $(this);
            var cantidad = +$btn.text();

            $("#iCantidad").val(cantidad);
            $("#btnCantidadComprobaciones button").removeClass("active");
            $btn.addClass("active");
        });

        //Eliminar factura virtual de transaccion
        $(".transaccion-porFacturar").on("click", function(e) {
            var id = $(this).data("relation");
            if (id) {
                navigator.notification.confirm(
                    "Esta seguro de eliminar la clasificación de la transaccion #" + id + " ?",
                    function(buttonIndex) {
                        if (buttonIndex === 1) {
                            Store.SetUntieInvoice(id).then(function(r) {
                                if (r.Success) {
                                    ctx.goToTransactions({
                                        refresh: true
                                    });
                                }
                            });
                        }
                    },
                    'Por Facturar', ['Si', 'No']
                );
            }
            return false;
        });
        //detalle de transaccion
        $(".transaccion").on("click", async function() {
            var TransactionId = $(this).data("id");

            ctx.Data.TransactionId = TransactionId;

            //Ligar factura virtual
            var data = {
                Type: "PendingToBill",
                InvoiceHeader: 1,
                TransactionId: TransactionId
            };

            Store.SetTieInvoice(data).then(async function(r) {
                console.log("SetTieInvoice", r);
                if (!r.Success) {
                    alert(r.Message);
                } else {
                    var RelationId = r.Data.RelationId;
                    //datos para clasificar...
                    var EmployeeId = ctx.Data.Employee.EmployeeId;
                    var metodosPago = await Store.PaymentMethods();
                    var conceptos = await Store.Catalogs(EmployeeId);
                    var presupuestos = await Store.GetBudgetsByTransaction(TransactionId);
                    var centroCostos = await Store.GetCostCenterByTransaction(TransactionId);
                    conceptos = Enumerable.from(conceptos.Data).select(function(el) {
                        return {
                            Text: el.Name,
                            Value: el.Id
                        }
                    }).toArray();

                    presupuestos = Enumerable.from(presupuestos.Data).select(function(el) {
                        return {
                            Text: el.Target,
                            Value: el.BudgetId
                        }
                    }).toArray();

                    var renderData = {
                        MetodosPago: metodosPago.Data,
                        Conceptos: conceptos,
                        Presupuestos: presupuestos,
                        CentroCostos: centroCostos.Data
                    };

                    ctx.renderPartial(porFacturarDetalleTpl, {
                        RelationId: RelationId,
                        Clasificar: renderData
                    }, ctx.handleEventsDetail);
                }
            });
        });
    },
    handleEventsDetail(ctx) {
        console.log("handleEventsDetail", ctx);
        ctx.Data.File = false;
        //Archivo transaccion manual
        $(".files64-button").on("click", function() {
            var $btn = $(this);
            var $container = $btn.parent(".files64");
            var pictureSource = $btn.data("source");

            app.getPicture(pictureSource).then(function(imageData) {
                console.log(imageData);
                $container.data("base", imageData);
                $container.find(".files64-prev-img").attr("src", "data:image/jpeg;base64," + imageData);
            }, function(err) {
                console.log(err);
            });

            ctx.Data.File = true;
        });
        //combo Clasificacion
        $("#cbConceptos").on("change", function() {
            var CadalogId = $(this).val();

            Store.SubCatalogs(CadalogId).then(function(r) {
                var clasificaciones = Enumerable.from(r.Data).select(function(el) {
                    return {
                        Text: el.Name,
                        Value: el.Id
                    }
                }).toArray();
                var options = Tool.renderTpl(optionsTpl, clasificaciones);

                $("#cbClasificacion").html(options);
            });
        });
        //combo centro de costos
        $("#cbClasificacion").on("change", function() {
            var SubCatalogId = $(this).val();
            var data = {
                EmployeeId: ctx.Data.Employee.EmployeeId,
                SubCatalogId: SubCatalogId,
                TransactionId: ctx.Data.TransactionId
            };

            Store.GetCostCenterBySubcatalog(data).then(function(r) {
                var centroCostos = r.Data;
                var options = Tool.renderTpl(optionsTpl, centroCostos);

                $("#cbCentroCostos").html(options);
            });
        });
        //Guardar Clasificacion
        $("#frm-setCatSubInvBalance").on("submit", function() {
            var $frm = $(this);
            var data = $frm.serializeObject();
            var asistente = $("#ck-asistente").is(":checked");

            console.log("Guardar Clasificacion::setCatSubInvBalance", data);

            //validacion asistente
            if (!ctx.Data.File) {}

            Store.SetCatSubInvBalance(data).then(function(r) {
                if (!r.Success) {
                    alert(r.Message);
                } else {
                    var TransactionId = ctx.Data.TransactionId;
                    var mensaje = "Listo. Se creo correctamente la comprobación.";
                    if (!ctx.Data.File) {
                        alert(mensaje);
                        app.View("main");
                    } else {
                        //si tomo una foto la gurda como imagen de la transaccion
                        var files64 = $("#files64").data("base");

                        var data = {
                            TransactionId: TransactionId,
                            File: files64
                        };

                        Store.SetFileTransaction(data).then(function(result) {
                            console.log("SetFileTransaction", result);
                            if (!result) {
                                alert(mensaje + "\nOcurrio un error al guardar el archivo.");
                            } else {
                                if (!asistente) {
                                    alert(mensaje);
                                    app.View("main");
                                } else {
                                    var dataFile = {
                                        EmployeeId: ctx.Data.Employee.EmployeeId,
                                        TransactionId: TransactionId,
                                        InvoiceId: 0,
                                        File: files64
                                    }
                                    Store.SetFileMultiTicket(dataFile).then(function(r) {
                                        alert(mensaje);
                                        app.View("main");
                                    });
                                }
                            }
                            //independientemente del resultado, refeccionar a main
                            app.View("main");
                        });
                    }
                }
            });
            return false;
        });
        //boton de volver
        $(".btnCerrar").on("click", function() {
            var $btn = $(this);
            var RelationId = $btn.data("relation");
            var type = $btn.data("type");

            Store.SetUntieInvoice(RelationId).then(function(r) {
                if (r.Success) {
                    if (type === "regresar") {
                        ctx.goToTransactions();
                    } else {
                        app.View("porFacturar");
                    }
                }
            });
        });
    },
    renderPartial(partial, data, callbackfunction) {
        var renderTpl = Tool.renderTpl(partial, data);

        $("#presupuestoAltaPartial").html(renderTpl);
        callbackfunction(this);
    },
    async goToTransactions(config) {
        var _this = this;
        var renderTpl = Tool.renderTpl(templateHtml);
        var _default = {
            refresh: false,
            data: [],
            TransactionId: 0
        }

        config = $.extend({}, _default, config);

        $("#renderBody").html(renderTpl);

        var fechaInicio = config.data.fechaInicio || moment().add(-15, "days").format("YYYY-MM-DD");
        var fechaFin = config.data.fechaFin || moment().format("YYYY-MM-DD");

        if (config.refresh) {
            var transacciones = await Store.Transactions({
                EmployeeId: _this.Data.Employee.EmployeeId,
                StartDate: fechaInicio,
                EndDate: fechaFin,
                Limit: config.data.Limit
            });
            _this.Data.Transacciones = Enumerable.from(transacciones.Data)
                .where("$.Balance > 0")
                .toArray();;
        }
        if (config.TransactionId) {
            _this.Data.TransactionId = config.TransactionId;
        }
        //Cargo la vista de transacciones...
        _this.Data.mensajeFiltros = config.mensajeFiltros;
        _this.renderPartial(porFacturarTransaccionesTpl, {
            Transacciones: _this.Data.Transacciones,
            TransactionId: config.TransactionId
        }, _this.handleEventsTransacciones);
    }
}