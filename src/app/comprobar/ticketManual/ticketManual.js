/**
 *
 * COMPROBAR - Ticket Manual
 *
 */
import "./ticketManual.less";
import templateHtml from "./ticketManual.tpl.html";
import optionsTpl from "../../shared/options.tpl.html";

import $ from 'jquery';
import Enumerable from 'linq';
import moment from 'moment';
import Tool from '../../utils/tool';
import Store from '../../store';
import Comprobar from '../comprobar';

export default {
    Data: {},
    init(Data) {
        this.Data = Data;
        this.render();
    },
    async render() {
        var _this = this;
        var fecha = moment().format('YYYY-MM-DD')
        var TransactionId = _this.Data.TransactionId;
        var EmployeeId = _this.Data.Employee.EmployeeId;
        var conceptos = await Store.Catalogs(EmployeeId);
        var presupuestos = await Store.GetBudgetsByTransaction(TransactionId);
        var centroCostos = await Store.GetCostCenterByTransaction(TransactionId);

        conceptos = Enumerable.from(conceptos.Data).select(function(el) {
            return {
                Text: el.Name,
                Value: el.Id
            }
        }).toArray();

        presupuestos = Enumerable.from(presupuestos.Data).select(function(el) {
            return {
                Text: el.Target,
                Value: el.BudgetId
            }
        }).toArray();

        var renderTpl = Tool.renderTpl(templateHtml, {
            Conceptos: conceptos,
            Presupuestos: presupuestos,
            CentroCostos: centroCostos.Data,
            Fecha: fecha,
            Amount: _this.Data.Amount,
            TransactionId: _this.Data.TransactionId
        });

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    handleEvents() {
        var _this = this;

        _this.Data.File = false;
        //combo Clasificacion
        $("#cbConceptos").on("change", function() {
            var CadalogId = $(this).val();

            Store.SubCatalogs(CadalogId).then(function(r) {
                var clasificaciones = Enumerable.from(r.Data).select(function(el) {
                    return {
                        Text: el.Name,
                        Value: el.Id
                    }
                }).toArray();
                var options = Tool.renderTpl(optionsTpl, clasificaciones);

                $("#cbClasificacion").html(options);
            });
        });
        //Archivo transaccion manual
        $(".files64-button").on("click", function() {
            var $btn = $(this);
            var $container = $btn.parent(".files64");
            var pictureSource = $btn.data("source");

            app.getPicture(pictureSource).then(function(imageData) {
                console.log(imageData);
                $container.data("base", imageData);
                $container.find(".files64-prev-img").attr("src", "data:image/jpeg;base64," + imageData);
            }, function(err) {
                console.log(err);
            });

            _this.Data.File = true;
        });
        //Guardar ticket manual
        $("#frm-setUploadManualTicket").on("submit", function() {
            var $frm = $(this);
            var data = $frm.serializeObject();

            console.log(data);

            Store.SetUploadManualTicket(data).then(function(r) {
                if (!r.Success) {
                    alert(r.Message);
                } else {
                    var TicketId = r.Data.HeaderId;
                    var mensaje = "Listo, se realizo correctamente la acción.\nTicket N°" + TicketId;
                    if (!_this.Data.File) {
                        alert(mensaje);
                        Comprobar.goToTransactions({
                            refresh: true
                        });
                    } else {
                        var files64 = $("#files64").data("base");

                        var data = {
                            TicketId: TicketId,
                            File: files64
                        };

                        Store.SetFileManualTicket(data).then(function(result) {
                            if (!result) {
                                alert(mensaje + "\nOcurrio un error al guardar el archivo.");
                            } else {
                                alert(mensaje);
                            }
                            Comprobar.goToTransactions({
                                refresh: true
                            });
                        });
                    }
                }
            });
            return false;
        });
        //boton de volver
        $(".btnVolver").on("click", function() {
            Comprobar.goToTransactions({
                refresh: false
            });
        });
    }
}