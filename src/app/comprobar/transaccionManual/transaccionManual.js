/**
 *
 * COMPROBAR - Transaccion Manual
 *
 */
import "./transaccionManual.less";
import templateHtml from "./transaccionManual.tpl.html";
import optionsTpl from "../../shared/options.tpl.html";

import $ from 'jquery';
import Enumerable from 'linq';
import moment from 'moment';
import Tool from '../../utils/tool';
import Store from '../../store';
import Comprobar from '../comprobar';

export default {
    Data: {},
    init(Data) {
        this.Data = Data;
        this.render();
    },
    async render() {
        var fecha = moment().format('YYYY-MM-DD')
        var EmployeeId = this.Data.Employee.EmployeeId;
        var metodosPago = await Store.PaymentMethods();
        var conceptos = await Store.Catalogs(EmployeeId);
        var presupuestos = await Store.GetBudgetsByEmployee({
            Operation: "CREATE",
            EmployeeId: EmployeeId,
            StartDate: fecha,
            EndDate: fecha
        });
        var centroCostos = await Store.GetCostCenterByEmployee(EmployeeId);

        conceptos = Enumerable.from(conceptos.Data).select(function(el) {
            return {
                Text: el.Name,
                Value: el.Id
            }
        }).toArray();

        presupuestos = Enumerable.from(presupuestos.Data).select(function(el) {
            return {
                Text: el.Target,
                Value: el.BudgetId
            }
        }).toArray();

        var renderTpl = Tool.renderTpl(templateHtml, {
            MetodosPago: metodosPago.Data,
            Conceptos: conceptos,
            Presupuestos: presupuestos,
            CentroCostos: centroCostos.Data,
            Fecha: fecha
        });

        $("#renderBody").html(renderTpl);

        this.handleEvents();

        console.log("DATA DE TRANSACCION MANUAL", this.Data);
    },
    handleEvents() {
        var _this = this;
        _this.Data.File = false;
        //Actualizo los presupuestos por fecha
        $("#dtFecha").on("change", async function() {
            var date = $(this).val();
            var EmployeeId = _this.Data.Employee.EmployeeId;

            if (date) {
                var presupuestos = await Store.GetBudgetsByEmployee({
                    Operation: "CREATE",
                    EmployeeId: EmployeeId,
                    StartDate: date,
                    EndDate: date
                });
                presupuestos = Enumerable.from(presupuestos.Data).select(function(el) {
                    return {
                        Text: el.Target,
                        Value: el.BudgetId
                    }
                }).toArray();

                var options = Tool.renderTpl(optionsTpl, presupuestos);

                $("#cbPresupuestos").html(options);
            }
        });
        //Archivo transaccion manual
        $(".files64-button").on("click", function() {
            var $btn = $(this);
            var $container = $btn.parent(".files64");
            var pictureSource = $btn.data("source");

            app.getPicture(pictureSource).then(function(imageData) {
                console.log(imageData);
                $container.data("base", imageData);
                $container.find(".files64-prev-img").attr("src", "data:image/jpeg;base64," + imageData);
            }, function(err) {
                console.log(err);
            });

            _this.Data.File = true;
        });
        //check copiar ticket manual
        $("#ckTicketManual").on("change", function() {
            var checked = $(this).is(":checked");

            if (checked) {
                $("#btnCollapse").attr("disabled", false);
                $("#copiaTicketManualContainer").collapse("show");
            } else {
                $("#btnCollapse").attr("disabled", true);
                $("#copiaTicketManualContainer").collapse("hide");
            }
        });
        //carga clasificacion
        $("#cbConceptos").on("change", function() {
            var CadalogId = $(this).val();

            Store.SubCatalogs(CadalogId).then(function(r) {
                var clasificaciones = Enumerable.from(r.Data).select(function(el) {
                    return {
                        Text: el.Name,
                        Value: el.Id
                    }
                }).toArray();
                var options = Tool.renderTpl(optionsTpl, clasificaciones);

                $("#cbClasificacion").html(options);
            });
        });
        //Guarda la transaccion manual
        $("#frm-setTransactionManual").on("submit", function() {
            var $frm = $(this);
            var data = $frm.serializeObject();

            data.EmployeeId = _this.Data.Employee.EmployeeId;

            Store.SetTransactionManual(data).then(function(r) {
                if (!r.Success) {
                    alert(r.Message);
                } else {
                    var HeaderId = r.Data.HeaderId;
                    var TransactionId = r.Data.TransactionId;
                    var mensaje = "Se creo correctamente la transaccion manual\nTransacción N°" + TransactionId;
                    if (!_this.Data.File) {
                        alert(mensaje);
                        Comprobar.goToTransactions({
                            refresh: true
                        });
                    } else {
                        var files64 = $("#files64").data("base");

                        var data = {
                            TicketId: HeaderId,
                            TransactionId: TransactionId,
                            File: files64
                        };
                        //Guardo en ticket
                        Store.SetFileTransaction(data).then(function(result) {
                            if (!result) {
                                alert(mensaje + "\nOcurrio un error al guardar el archivo.");
                            } else {
                                var copiarATicketManual = $("#ckTicketManual").is(":checked");
                                if (copiarATicketManual) {
                                    //Guardo en no deducibles
                                    Store.SetFileManualTicket(data).then(function(result) {
                                        if (!result) {
                                            alert(mensaje + "\nOcurrio un error al guardar el archivo.");
                                        } else {
                                            alert(mensaje);
                                        }
                                        Comprobar.goToTransactions({
                                            refresh: true
                                        });
                                    });
                                } else {
                                    alert(mensaje);
                                    Comprobar.goToTransactions({
                                        refresh: true
                                    });
                                }
                            }
                        });
                    }
                }
            });

            return false;
        });
        //boton de volver
        $(".btnVolver").on("click", function() {
            Comprobar.goToTransactions({
                refresh: false
            });
        });
    }
}