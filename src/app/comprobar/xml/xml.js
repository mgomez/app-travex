/**
 *
 * COMPROBAR - Transaccion Manual
 *
 */
import "./xml.less";
import templateHtml from "./xml.tpl.html";
import clasificacionTpl from "./_xml-clasificacion.tpl.html";
import optionsTpl from "../../shared/options.tpl.html";

import $ from 'jquery';
import Enumerable from 'linq';
import moment from 'moment';
import Jets from 'jets';
import Tool from '../../utils/tool';
import Store from '../../store';
import Comprobar from '../comprobar';

export default {
    Data: {},
    init(Data) {
        this.Data = Data;
        this.render();
    },
    async render() {
        var _this = this;

        var renderTpl = Tool.renderTpl(templateHtml, {
            Facturas: _this.Data.Facturas
        });

        $("#renderBody").html(renderTpl);

        this.handleEvents();

        console.log("DATA DE XML", this.Data);
    },
    handleEvents() {
        var _this = this;
        //buscador de facturas
        $("style").remove();
        var jets = new Jets({
            searchTag: '#jetsSearch',
            contentTag: '#facturasContainer'
        });
        //Ligar factura con transaccion
        $(".xml").on("click", function() {
            var InvoiceHeader = $(this).data("id");
            var TransactionId = _this.Data.TransactionId;
            var data = {
                Type: "Classificate",
                InvoiceHeader: InvoiceHeader,
                TransactionId: TransactionId
            };

            Store.SetTieInvoice(data).then(async function(r) {
                console.log("SetTieInvoice", r);
                if (!r.Success) {
                    alert(r.Message);
                } else {
                    window.plugins.toast.show('La transacción se ha ligado a la factura correctamente!', 'long', 'center');
                    var RelationId = r.Data.RelationId;
                    //datos para clasificar...
                    var EmployeeId = _this.Data.Employee.EmployeeId;
                    var metodosPago = await Store.PaymentMethods();
                    var conceptos = await Store.Catalogs(EmployeeId);
                    var presupuestos = await Store.GetBudgetsByTransaction(TransactionId);
                    var centroCostos = await Store.GetCostCenterByTransaction(TransactionId);
                    conceptos = Enumerable.from(conceptos.Data).select(function(el) {
                        return {
                            Text: el.Name,
                            Value: el.Id
                        }
                    }).toArray();

                    presupuestos = Enumerable.from(presupuestos.Data).select(function(el) {
                        return {
                            Text: el.Target,
                            Value: el.BudgetId
                        }
                    }).toArray();

                    var renderData = {
                        MetodosPago: metodosPago.Data,
                        Conceptos: conceptos,
                        Presupuestos: presupuestos,
                        CentroCostos: centroCostos.Data
                    };

                    _this.renderPartial(clasificacionTpl, {
                        RelationId: RelationId,
                        Clasificar: renderData
                    }, _this.handleEventsDetail);
                }
            });
        });
        //boton de volver
        $(".btnVolver").on("click", function() {
            Comprobar.goToTransactions();
        });
    },
    handleEventsDetail(ctx) {
        console.log("handleEventsDetail", ctx);
        //combo Clasificacion
        $("#cbConceptos").on("change", function() {
            var CadalogId = $(this).val();

            Store.SubCatalogs(CadalogId).then(function(r) {
                var clasificaciones = Enumerable.from(r.Data).select(function(el) {
                    return {
                        Text: el.Name,
                        Value: el.Id
                    }
                }).toArray();
                var options = Tool.renderTpl(optionsTpl, clasificaciones);

                $("#cbClasificacion").html(options);
            });
        });
        //combo centro de costos
        $("#cbClasificacion").on("change", function() {
            var SubCatalogId = $(this).val();
            var data = {
                EmployeeId: ctx.Data.Employee.EmployeeId,
                SubCatalogId: SubCatalogId,
                TransactionId: ctx.Data.TransactionId
            };

            Store.GetCostCenterBySubcatalog(data).then(function(r) {
                var centroCostos = r.Data;
                var options = Tool.renderTpl(optionsTpl, centroCostos);

                $("#cbCentroCostos").html(options);
            });
        });
        //Guardar Clasificacion
        $("#frm-setCatSubInvBalance").on("submit", function() {
            var $frm = $(this);
            var data = $frm.serializeObject();

            console.log(data);

            Store.SetCatSubInvBalance(data).then(function(r) {
                console.log("SetCatSubInvBalance", r);
                if (r.Success) {
                    app.View("main");
                    alert("¡Listo, se ha clasificado la factura correctamente!");
                }
            });
            return false;
        });
        //boton de volver
        $(".btnGoToTransactions").on("click", function() {
            ctx.goToTransactions();
        });
    },
    renderPartial(partial, data, callbackfunction) {
        var renderTpl = Tool.renderTpl(partial, data);

        $("#renderBody").html(renderTpl);
        callbackfunction(this);
    },
    async goToTransactions(refresh) {
        var _this = this;
        var renderTpl = Tool.renderTpl(templateHtml);

        $("#renderBody").html(renderTpl);
        if (refresh) {
            var transacciones = await Store.Transactions(_this.Data.Employee.EmployeeId);
            _this.Data.Transacciones = transacciones.Data;
        }
        //Cargo la vista de transacciones...
        _this.renderPartial(porFacturarTransaccionesTpl, {
            Transacciones: _this.Data.Transacciones
        }, _this.handleEventsTransacciones);
    }
}