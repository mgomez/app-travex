/**
 *
 * COMPROBAR
 *
 */
import "./comprobar.less";
import templateHtml from "./comprobar.tpl.html";

import $ from 'jquery';
import localforage from 'localforage';
import Enumerable from 'linq';
import Jets from 'jets';
import moment from 'moment';
import Tool from '../utils/tool';
import Store from '../store';
//Parciales
import optionsTpl from "../shared/options.tpl.html";
import _empleados from "../shared/_empleados/_empleados";
import comprobarTransaccionesTpl from "./comprobar-transacciones.tpl.html";
import comprobarCollapseTpl from "./comprobar-collapse.tpl.html";
import comprobarDetalleTpl from "./comprobar-detalle.tpl.html";
import transaccionManual from "./transaccionManual/transaccionManual";
import ticketManual from "./ticketManual/ticketManual";
import XML from "./xml/xml";

function periodoValido(data) {
    var fechaInicio = moment(data.fechaInicio);
    var fechaFin = moment(data.fechaFin);

    if (fechaFin < fechaInicio) {
        return false;
    }

    return true;
}

export default {
    Data: {},
    init() {
        this.Data.Employee = {};
        this.Data.TransactionId = null;
        this.render();
    },
    async render() {
        var empleados = await _empleados.render("Expenses");

        var renderTpl = Tool.renderTpl(templateHtml, {
            Partial: empleados
        });

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    handleEvents() {
        var _this = this;
        //inicializa la funcionalidad de la vista de empleados...
        _empleados.handleEvents();

        $(".empleado").on("click", async function() {
            var $this = $(this);
            var EmployeeId = $this.data("id");
            var FullName = $this.data("name");

            _this.Data.Employee.EmployeeId = EmployeeId;
            _this.Data.Employee.FullName = FullName;

            var Transacciones = await Store.Transactions({
                EmployeeId: EmployeeId,
                StartDate: moment().add(-15, "days").format("YYYY-MM-DD"),
                EndDate: moment().format("YYYY-MM-DD"),
                Limit: 10
            });

            _this.Data.Transacciones = Transacciones.Data;
            _this.Data.mensajeFiltros = "*transacciones de los ultimos 15 días (10 max)";
            //Cargo la vista de transacciones...
            _this.renderPartial(comprobarTransaccionesTpl, {
                Transacciones: Transacciones.Data
            }, _this.handleEventsTransacciones);
        });

        _empleados.empleadoDefault();
    },
    handleEventsTransacciones(ctx) {
        moment.locale("es");
        var mensajeFiltros = ctx.Data.mensajeFiltros || "*transacciones de los ultimos 15 días (10 max)";

        $("#mensajeFiltros").html(mensajeFiltros);
        //Buscardor de transacciones
        $("style").remove();

        var jets = new Jets({
            searchTag: '#jetsSearch',
            contentTag: '#transaccionesContainer',
            callSearchManually: true
        });
        //Filtros
        $("#modalFiltros").on("show.bs.modal", function() {
            var fechaInicio = moment().add(-15, 'days').format("YYYY-MM-DD");
            var fechaFin = moment().format("YYYY-MM-DD");

            $("#fechaInicio").val(fechaInicio);
            $("#fechaFin").val(fechaFin);
        });

        $(".dropdown-item").on("click", function() {
            var option = $(this).data("value");

            var fechaInicio = "";
            var fechaFin = "";

            switch (option) {
                case 1:
                default:
                    fechaInicio = moment().format("YYYY-MM-DD");
                    fechaFin = fechaInicio;
                    break;
                case 2:
                    fechaInicio = moment().add(-7, "days").format("YYYY-MM-DD");
                    fechaFin = moment().format("YYYY-MM-DD");
                    break;
                case 3:
                    fechaInicio = moment().startOf("month").format("YYYY-MM-DD");
                    fechaFin = moment().endOf("month").format("YYYY-MM-DD");
                    break;
                case 4:
                    fechaInicio = moment().add(-90, "days").format("YYYY-MM-DD");
                    fechaFin = moment().format("YYYY-MM-DD");
                    break;
            }

            $("#fechaInicio").val(fechaInicio);
            $("#fechaFin").val(fechaFin);
        });

        $("#frmFiltros").on("submit", function() {
            var $frm = $(this);
            var data = $frm.serializeObject();

            console.log(data);

            if (periodoValido(data)) {
                $("#modalFiltros").modal("hide");
                var formatDate = "ddd DD MMMM";
                mensajeFiltros = "*Ultimas " + data.Limit + " transacciones.<br>" + moment(data.fechaInicio).format(formatDate) + " - " + moment(data.fechaFin).format(formatDate);

                ctx.goToTransactions({
                    refresh: true,
                    data: data,
                    mensajeFiltros: mensajeFiltros
                });
            } else {
                alert("La fecha fin debe ser mayor a la fecha de inicio.");
            }
            return false;
        });

        $("#btnCantidadComprobaciones button").on("click", function() {
            var $btn = $(this);
            var cantidad = +$btn.text();

            $("#iCantidad").val(cantidad);
            $("#btnCantidadComprobaciones button").removeClass("active");
            $btn.addClass("active");
        });
        //& manual
        $("#jetsSearch").on("keyup", function() {
            var q = $(this).val();
            jets.search(q);
        });
        //& botones
        var $filtros = $(".transacciones-filtros-item");
        //contador
        var txTotal = ctx.Data.Transacciones.length;
        var txVerde = Enumerable.from(ctx.Data.Transacciones).where(function(el) {
            return el.Color.Name == "VERDE";
        }).count();
        var txRojo = Enumerable.from(ctx.Data.Transacciones).where(function(el) {
            return el.Color.Name == "ROJO";
        }).count();
        var txAzul = Enumerable.from(ctx.Data.Transacciones).where(function(el) {
            return el.Color.Name == "AZUL";
        }).count();
        $(".transacciones-filtros-item.active").text(txTotal);
        $(".transacciones-filtros-item.verde").text(txVerde);
        $(".transacciones-filtros-item.azul").text(txAzul);
        $(".transacciones-filtros-item.rojo").text(txRojo);

        $filtros.on("click", function() {
            var $selected = $(this);
            var filter = $selected.data("filter");

            $filtros.removeClass('active');

            $selected.addClass('active');
            jets.search(filter);

            $(".transacciones-filtrosDetalle span").removeClass('active');
            var klass = $selected[0].className;
            klass = klass.replace(/transacciones-filtros-item|active|\s/g, "");
            klass = !klass ? "na" : klass;

            $(".transacciones-filtrosDetalle span").filter(function(i, el) {
                return el.className === klass;
            }).addClass('active');

            $(".transacciones-filtrosDetalle").toggleClass(klass);
        });
        //voy hasta la factura que modifico
        var $transaccionActual = $("#trnx-" + ctx.Data.TransactionId);
        if ($transaccionActual.length > 0) {
            var elOffset = $transaccionActual.offset().top;
            var elHeight = $transaccionActual.height();
            var windowHeight = $(window).height();
            var offset;

            if (elHeight < windowHeight) {
                offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
            } else {
                offset = elOffset;
            }
            $("html, body")
                .delay(200)
                .animate({
                    scrollTop: offset
                }, 600);
            $transaccionActual.addClass('selected');
            setTimeout(function() {
                $transaccionActual.removeClass('selected');
            }, 10000);
        }
        //boton de actualizar
        $("#btnUpdate").on("click", function() {
            ctx.goToTransactions({
                refresh: true
            });
        });
        //boton de transaccion manual
        $("#btnTransaccionManual").on("click", function() {
            transaccionManual.init(ctx.Data);
        });
        //eliminar transaccion(manual)
        $(".transaccion-eliminar").on("click", function(e) {
            var id = $(this).data("id");

            navigator.notification.confirm(
                "Esta seguro de eliminar la transacción manual " + id + " ?",
                function(buttonIndex) {
                    if (buttonIndex === 1) {
                        Store.SetDeleteTransaction(id).then(function() {
                            alert("Listo");
                            ctx.goToTransactions({
                                refresh: true
                            });
                        });
                    }
                },
                'Comprobar', ['Si', 'No']
            );
            return false;
        });
        //detalle de transaccion
        $(".transaccion").on("click", async function() {
            var $btn = $(this);
            var isDisableCollapsButtons = $btn.hasClass('GRIS');
            var TransactionId = $(this).data("id");

            ctx.Data.TransactionId = TransactionId;

            var fecha = moment().format('YYYY-MM-DD')
            var EmployeeId = ctx.Data.Employee.EmployeeId;
            var metodosPago = await Store.PaymentMethods();
            var conceptos = await Store.Catalogs(EmployeeId);
            var presupuestos = await Store.GetBudgetsByTransaction(TransactionId);
            var centroCostos = await Store.GetCostCenterByTransaction(TransactionId);

            conceptos = Enumerable.from(conceptos.Data).select(function(el) {
                return {
                    Text: el.Name,
                    Value: el.Id
                }
            }).toArray();

            presupuestos = Enumerable.from(presupuestos.Data).select(function(el) {
                return {
                    Text: el.Target,
                    Value: el.BudgetId
                }
            }).toArray();

            var renderData = {
                MetodosPago: metodosPago.Data,
                Conceptos: conceptos,
                Presupuestos: presupuestos,
                CentroCostos: centroCostos.Data
            };

            Store.GetInvoicebyTransaction(TransactionId).then(function(r) {
                var facturas = r.Data;
                var transaccion = Enumerable.from(ctx.Data.Transacciones).where("$.Id ==" + TransactionId).firstOrDefault();

                ctx.renderPartial(comprobarDetalleTpl, {
                    Facturas: facturas,
                    Transaccion: transaccion,
                    showJets: facturas.length === 0 ? "hide" : "",
                    showOptions: transaccion.Balance !== 0 && !transaccion.IsVirtual,
                    IsVirtual: transaccion.IsVirtual,
                    Clasificar: renderData,
                    isDisableCollapsButtons: isDisableCollapsButtons
                }, ctx.handleEventsDetail);
            });
        });
    },
    handleEventsDetail(ctx) {

        //Buscardor de facturas
        $("style").remove();
        var jets = new Jets({
            searchTag: '#jetsSearch',
            contentTag: '#facturasContainer'
        });
        //boton XML
        $(".btnXML").on("click", function() {
            Store.GetInvoicebyEmployee({
                EmployeeId: ctx.Data.Employee.EmployeeId
            }).then(function(r) {
                ctx.Data.Facturas = r.Data;
                console.log(r);
                XML.init(ctx.Data);
            });
        });
        //boton de ticket manual a factura
        $(".btnTicketManual").on("click", function() {
            var Amount = $(this).data("amount");
            console.log("click");
            ctx.Data.Amount = Amount;
            ticketManual.init(ctx.Data);
        });
        //detalle factura
        $(".factura").on("click", function() {
            var $collapse = $(this);
            var disabledButtons = $collapse.data("disabled");
            var RelationId = $collapse.data("id");
            var $container = $collapse.next(".collapse");
            var data = {
                RelationId: RelationId,
                TransactionId: ctx.Data.TransactionId,
            };

            if (!$container.hasClass('show')) {
                Store.GetDetailRelation(data).then(function(r) {
                    var DetalleFactura = Enumerable.from(r.Data).select(function(el) {
                        el.isPptoNA = el.Ppto.Id === "1000";
                        el.detalleOtraTransaccion = el.TransactionId !== data.TransactionId;
                        return el;
                    }).toArray();
                    DetalleFactura.RelationId = Enumerable.from(DetalleFactura).where(function(el) {
                        return el.TransactionId === data.TransactionId
                    }).firstOrDefault().RelationId;
                    DetalleFactura.disabledButtons = !disabledButtons;

                    console.log("DetalleFactura", DetalleFactura);

                    var renderTpl = Tool.renderTpl(comprobarCollapseTpl, DetalleFactura);

                    $container.html(renderTpl);

                    //Desligar factura
                    $(".btnSetUntieInvoice").on("click", function() {
                        var $btn = $(this);
                        var RelationId = $btn.data("id");

                        Store.SetUntieInvoice(RelationId).then(function(r) {
                            console.log(r);
                            alert("Listo");
                            ctx.goToTransactions({
                                refresh: true
                            });
                        });
                    });
                    //Clasificar factura
                    $(".btnSetCatSubInvBalance").on("click", function() {
                        var $btn = $(this);
                        var RelationId = $btn.data("id");
                        var BudgetId = $btn.data("budget");

                        $("#clasificarContainer")
                            .addClass('active')
                            .find(".RelationId").val(RelationId);

                        $("#cbPresupuestos option").filter(function(index, el) {
                            return +$(el).val() === BudgetId;
                        }).attr("selected", true);
                    });
                    //Ver transaccion
                    $(".goToTransactions").on("click", function() {
                        var TransactionId = $(this).data("transaction");
                        ctx.goToTransactions({
                            refresh: false,
                            TransactionId: TransactionId
                        });
                    })
                });
            }
        });
        //combo Clasificacion
        $("#cbConceptos").on("change", function() {
            var CadalogId = $(this).val();

            Store.SubCatalogs(CadalogId).then(function(r) {
                var clasificaciones = Enumerable.from(r.Data).select(function(el) {
                    return {
                        Text: el.Name,
                        Value: el.Id
                    }
                }).toArray();
                var options = Tool.renderTpl(optionsTpl, clasificaciones);

                $("#cbClasificacion").html(options);
            });
        });
        //combo centro de costos
        $("#cbClasificacion").on("change", function() {
            var SubCatalogId = $(this).val();
            var data = {
                EmployeeId: ctx.Data.Employee.EmployeeId,
                SubCatalogId: SubCatalogId,
                TransactionId: ctx.Data.TransactionId
            };

            Store.GetCostCenterBySubcatalog(data).then(function(r) {
                var centroCostos = r.Data;
                var options = Tool.renderTpl(optionsTpl, centroCostos);

                $("#cbCentroCostos").html(options);
            });
        });
        //Guardar Clasificacion
        $("#frm-setCatSubInvBalance").on("submit", function() {
            var $frm = $(this);
            var data = $frm.serializeObject();
            var $btnSubmit = $frm.find("button[type=submit]");

            console.log("SetCatSubInvBalance", data);

            $btnSubmit.attr("disabled", true);

            Store.SetCatSubInvBalance(data).then(function(r) {
                console.log("SetCatSubInvBalance", r);
                if (r.Success) {
                    ctx.goToTransactions({
                        refresh: false,
                        TransactionId: TransactionId
                    });
                    alert("Listo, se realizo correctamente la acción.");
                }
            }, function(err) {
                $btnSubmit.attr("disabled", false);
            });
            return false;
        });
        //boton cerrar panel
        $(".btnCerrar").on("click", function() {
            $("#clasificarContainer").removeClass('active');
        });
        //boton de volver
        $(".btnGoToTransactions").on("click", function() {
            ctx.goToTransactions({
                refresh: false
            });
        });
    },
    renderPartial(partial, data, callbackfunction) {
        console.log("renderPartial ", data);
        var renderTpl = Tool.renderTpl(partial, data);

        $("#presupuestoAltaPartial").html(renderTpl);
        callbackfunction(this);
    },
    async goToTransactions(config) {
        var _this = this;
        var renderTpl = Tool.renderTpl(templateHtml);
        var _default = {
            refresh: false,
            data: [],
            TransactionId: 0
        }

        config = $.extend({}, _default, config);

        console.log(config);

        $("#renderBody").html(renderTpl);

        var fechaInicio = config.data.fechaInicio || moment().add(-15, "days").format("YYYY-MM-DD");
        var fechaFin = config.data.fechaFin || moment().format("YYYY-MM-DD");

        if (config.refresh) {
            var transacciones = await Store.Transactions({
                EmployeeId: _this.Data.Employee.EmployeeId,
                StartDate: fechaInicio,
                EndDate: fechaFin,
                Limit: config.data.Limit
            });
            _this.Data.Transacciones = transacciones.Data;
        }
        if (config.TransactionId) {
            _this.Data.TransactionId = config.TransactionId;
        }
        //Cargo la vista de transacciones...
        _this.Data.mensajeFiltros = config.mensajeFiltros;
        _this.renderPartial(comprobarTransaccionesTpl, {
            Transacciones: _this.Data.Transacciones,
            TransactionId: config.TransactionId
        }, _this.handleEventsTransacciones);
    }
}