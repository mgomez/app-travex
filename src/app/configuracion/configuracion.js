/**
 *
 * Configuracion
 *
 */
import "./configuracion.less";
import $ from 'jquery';
import jqueryValidation from 'jquery-validation';
import localforage from 'localforage';
import Tool from '../utils/tool';
import Store from '../store';
import templateHtml from "./configuracion.tpl.html";

function validaFormato(val) {
    var mensaje = "FORMATO INCORRECTO\n";
    var minuscula = /[a-z].*/.test(val);
    var mayuscula = /[A-Z].*/.test(val);
    var numero = /\d/.test(val);
    var especial = /[#$^+=!*()@%&]/.test(val);
    var longitud = val.length >= 6;

    if (!minuscula)
        mensaje += "Debe contener una minúscula.\n";
    if (!mayuscula)
        mensaje += "Debe contener una mayúscula.\n";
    if (!numero)
        mensaje += "Debe contener un numero.\n";
    if (!especial)
        mensaje += "Debe contener al menos un carácter especial( #$^+=!*()@%& ).\n";
    if (!longitud)
        mensaje += "La contraseña debe ser mínimo de 6 dígitos.";


    if (!minuscula || !mayuscula || !numero || !especial || !longitud) {
        alert(mensaje);
    }
}

export default {
    init() {
        this.render();
    },
    render() {
        var renderTpl = Tool.renderTpl(templateHtml);

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    handleEvents() {
        //muestra la contraseña
        $(".togglePassword").on("click", function() {
            var $btn = $(this);
            var $inputPassword = $btn.next("input");

            $btn.toggleClass('active');

            if ($btn.hasClass('active')) {
                $inputPassword.attr("type", "text");
            } else {
                $inputPassword.attr("type", "password");
            }
        });
        //seguridad de contraseña
        $("#inputPassword").on("keyup", function() {
            var val = $(this).val();
            var className = "formRegistro-passwordCheck ";

            var low = /([a-z]|[A-Z]|\d|[#$^+=!*()@%&]).{2,}/.test(val);
            var middle = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{5,}$/.test(val);
            var heavy = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,}$/.test(val);

            var level = !low ? 0 : !middle ? 1 : !heavy ? 2 : 3;

            switch (level) {
                case 1:
                    className += "low";
                    break;
                case 2:
                    className += "middle";
                    break;
                case 3:
                    className += "heavy";
                    break;
                default:
                    break;
            }

            $("#passwordCheck")[0].className = className;
        }).on("blur", function() {
            var val = $(this).val();

            validaFormato(val);
        });

        $("#frm-ChangePassword").on("submit", function() {
            var $frm = $(this);
            var formData = $frm.serializeObject();

            if ($frm.valid()) {
                Store.ChangePassword(formData).then(function(r) {
                    alert('Se cambió correctamente la contraseña.');
                    app.View("main");
                });
            }

            return false;
        }).validate({
            'rules': {
                'NewPassword': {
                    required: true,
                    minlength: 6
                },
                'ConfirmPassword': {
                    required: true,
                    equalTo: '#inputPassword'
                }
            },
            'messages': {
                'OldPassword': {
                    required: 'Ingresa tu contraseña actual.'
                },
                'NewPassword': {
                    required: 'Ingresa una contraseña nueva.',
                    minlength: 'La contraseña debe ser mínimo de 6 caracteres.'
                },
                'ConfirmPassword': {
                    required: 'Ingresa nuevamente la contraseña.',
                    equalTo: 'La nueva contraseña no coincide.'
                }
            }
        });
    }
};