/**
 *
 * INDEX
 *
 */
import 'bootstrap/dist/css/bootstrap.css';
import "../assets/css/app.less";
import FastClick from "fastclick";
import $ from 'jquery';
import localforage from 'localforage';
import Router from './router';

import "../assets/css/touch-sideswipe.css";
import TouchSideSwipe from "../assets/js/touch-sideswipe.min.js";

require("bootstrap");

window.jQuery = $;

window.Connection = true;

localforage.config({
    driver: localforage.LOCALSTORAGE,
    name: 'Travex',
    version: 0.1,
    size: 4980736,
    storeName: 'dbOcInvoice',
});

var config = {
    elementID: 'touchSideSwipe',
    elementWidth: 400, //px
    elementMaxWidth: 0.8, // *100%
    sideHookWidth: 44, //px
    moveSpeed: 0.2, //sec
    opacityBackground: 0.8,
    shiftForStart: 50, // px
    windowMaxWidth: 1024, // px
}
window.touchSideSwipe = new TouchSideSwipe(config);

window.app = {
    initialize: function() {
        console.log("initialize");
        //Polyfill para eliminar retrasos de clics en los navegadores con IU táctiles
        FastClick.attach(document.body);
        //nombre de archivo en los inputs file
        $(document).on("change", ".input-file", function(element) {
            var $input = $(this),
                $label = $input.next('.js-labelFile'),
                labelVal = $label.html();

            var fileName = '';
            if (element.target.value) fileName = element.target.value.split('\\').pop();
            fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
        });

        document.addEventListener('deviceready', this.deviceReady, false);

        this.isAuthenticated();
    },
    deviceReady() {
        //agrega padding si es ios
        $("body").addClass(device.platform);
        //reemplaza notificaciones por nativo
        if (navigator.notification) {
            window.alert = function(message) {
                navigator.vibrate(100);
                navigator.notification.alert(message, null, "TRAVEX", 'OK');
            };
        }
        //valida conexion
        var networkState = navigator.connection.type;

        window.Connection = (networkState !== Connection.NONE) ? true : false;

        document.addEventListener("offline", function() {
            window.Connection = false;
        }, false);

        document.addEventListener("online", function() {
            window.Connection = true;
        }, false);
        //obtengo el registrationId para los mensajes push
        //app.setupPush();
    },
    setupPush() {
        var push = PushNotification.init({
            "android": {
                "senderID": "656330711133"
            },
            "browser": {},
            "ios": {
                "sound": true,
                "vibration": true,
                "badge": true
            },
            "windows": {}
        });

        push.on('registration', function(data) {
            console.log("deviceId", data.registrationId);
            localforage.setItem('deviceId', data.registrationId);
        });

        push.on('error', function(e) {
            alert("Ocurrió un error: " + e.message);
        });

        push.on('notification', function(data) {
            var origin = (data.additionalData) ? data.additionalData.origin : "";

            console.log("Push Result", data);

            switch (origin) {
                default: alert(data.message);
                break;
            }
        });

    },
    getPicture(pictureSource) {
        var Deferred = $.Deferred();
        try {
            var PictureSourceType = Camera.PictureSourceType[pictureSource];
            var config = {
                quality: (device.platform !== 'Android') ? 50 : 80,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: PictureSourceType,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 750,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            navigator.camera.getPicture(function(imageData) {
                //$img.attr("src", "data:image/jpeg;base64," + imageData);
                Deferred.resolve(imageData);
            }, function(message) {
                return Deferred.reject(message);
            }, config);

            return Deferred.promise();
        } catch (e) {
            return Deferred.reject(e);
        }
    },
    async isAuthenticated() {
        var User = await localforage.getItem('User');

        //valida que exista el Usuario en la memoria del telefono y que este verificado
        if (User) {
            $(".navbar-name").html(User.userName);
            Router.View('main');
        } else {
            Router.View('login', true);
        }
        this.loading(false);
    },
    loading(display) {
        if (display) {
            $("#loading").addClass('active');
        } else {
            $("#loading").removeClass('active');
        }
    },
    loadingXHR(display) {
        if (display) {
            $("#loadingXHR").addClass('active');
        } else {
            $("#loadingXHR").removeClass('active');
        }
    },
    LogOut() {
        const Promesas = [
            localforage.removeItem('frmRegistro'),
            localforage.removeItem('cellPhone'),
            localforage.removeItem('PersonalInfo'),
            localforage.removeItem('User'),
            localforage.removeItem('lastView')
        ]

        Promise.all(Promesas).then(function(result) {
            Router.View('login', true);
            window.touchSideSwipe.tssClose();
        });
    },
    View(view, _default) {
        Router.View(view, _default);
        window.touchSideSwipe.tssClose();
    }
};

app.initialize();

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

export default {

};