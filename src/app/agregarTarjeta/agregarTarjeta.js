/**
 *
 * Agregar Tarjeta
 *
 */
import "./agregarTarjeta.less";
import $ from 'jquery';
import jqueryValidation from 'jquery-validation';
import localforage from 'localforage';
import Tool from '../utils/tool';
import Store from '../store';
import templateHtml from "./agregarTarjeta.tpl.html";

export default {
    init() {
        this.render();
    },
    render() {
        var renderTpl = Tool.renderTpl(templateHtml);

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    handleEvents() {

        jQuery.validator.addMethod("exactlength", function(value, element, param) {
            return this.optional(element) || value.length == param;
        }, $.validator.format("Introduce únicamente {0} caracteres."));

        $('#frm-agregarTarjeta').on("submit", function() {
            var $frm = $(this);
            var formData = $frm.serializeObject();

            if ($frm.valid()) {
                Store.AddCard(formData).then(function(r) {
                    alert('Se agregó una nueva tarjeta.');
                    $frm[0].reset();
                });
            }

            return false;
        }).validate({
            'rules': {
                'Card': {
                    exactlength: 16
                }
            },
            'messages': {
                'Card': {
                    required: 'Ingresa una tarjeta.',
                    number: 'Ingresa una tarjeta válida.',
                    exactlength: 'Es necesario ingresar los 16 dígitos de la Tarjeta'
                }
            }
        })
    }
}