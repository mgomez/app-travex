/**
 *
 * STORE
 *
 */
import $ from 'jquery';
import localforage from 'localforage';
import Enumerable from 'linq';
import Constant from './utils/constant';
import Xhr from './utils/xhr';

export default {
    //Datos del usuario de la guardaos en localforage
    GetUser() {
        return localforage.getItem("User");
    },
    GetDeviceId() {
        return localforage.getItem("registrationId");
    },
    //Guarda el id del dispositivo para enviar las notificaciones push.
    //[deviceId],[systemDevice]
    async SaveDeviceData(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Profile/SaveDeviceData',
            'type': 'POST',
            'data': data,
            'access_token': User.access_token
        });
    },
    //Login
    Login(data) {
        return Xhr.ajax({
            url: 'https://www.ocsi.mx/Extranet/Services/WebApi_OneCardApp/token',
            type: 'POST',
            dataType: 'json',
            data: data,
        });
    },
    //Registro
    //[Email]
    //[Password]
    //[ConfirmPassword]
    //[Card]
    Register(data) {
        return Xhr.ajax({
            'path': '/api/Account/Register',
            'type': 'POST',
            'data': data
        });
    },
    //SendMailForgotPassword
    SendMailForgotPassword(data) {
        return Xhr.ajax({
            'path': '/api/Account/SendMailForgotPassword',
            'type': 'POST',
            'data': data
        });
    },
    //Saldo    
    async Cards(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Cards',
            'type': 'GET',
            'data': data,
            'access_token': User.access_token
        });
    },
    //Bloqueo de tarjetas    
    async SetAppCardONOFF(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Cards/Status',
            'type': 'POST',
            'data': data,
            'access_token': User.access_token
        });
    },
    //Cambio de contraseña
    async ChangePassword(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/ChangePassword',
            'type': 'POST',
            'data': data,
            'access_token': User.access_token
        });
    },
    //olvide contraseña...
    async ForgotPassword(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/ForgotPassword',
            'type': 'POST',
            'data': data
        });
    },
    //Empleados 
    async Users(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/Users',
            'type': 'GET',
            'data': data,
            'access_token': User.access_token
        });
    },
    //Guarda presupuesto EXPRESS
    async Express(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Budgets/Express',
            'type': 'POST',
            'data': data,
            'access_token': User.access_token
        });
    },
    //Agregar tarjeta
    async AddCard(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/AddCard',
            'type': 'POST',
            'data': data,
            'access_token': User.access_token
        });
    },
    //Lista de tarjetas para bloqueo
    async List(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Card/List',
            'type': 'GET',
            'data': data,
            'access_token': User.access_token
        });
    },
    //Cambia el estatus de una tarjeta
    async Status(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Card/Status',
            'type': 'POST',
            'data': data,
            'access_token': User.access_token
        });
    },
    //Proyectos
    async Projects(EmployeeId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/Users/' + EmployeeId + '/Projects',
            'type': 'GET',
            'access_token': User.access_token
        });
    },
    //Estados    
    async States() {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/States',
            'type': 'GET',
            'access_token': User.access_token
        });
    },
    //Municipios    
    async Municipalities(StateId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/States/' + StateId + '/Municipalities',
            'type': 'GET',
            'access_token': User.access_token
        });
    },
    //Conceptos    
    async Catalogs(EmployeeId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/' + EmployeeId + '/Catalogs',
            'type': 'GET',
            'access_token': User.access_token
        });
    },
    //Clasificacion    
    async SubCatalogs(CadalogId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/Catalogs/' + CadalogId + '/SubCatalogs',
            'type': 'GET',
            'access_token': User.access_token
        });
    },
    //Centro de Costos por empleado
    async GetCostCenterByEmployee(EmployeeId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/' + EmployeeId + '/CostCenters',
            'type': 'GET',
            'access_token': User.access_token
        });
    },
    //Centro de costos por transaccion
    async GetCostCenterByTransaction(TransactionId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/CostCenters/' + TransactionId,
            'type': 'GET',
            'access_token': User.access_token
        });
    },
    //Permisos
    //Budget/BudgetExpress/Authorizer
    async Pass(ToProcess) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/Pass',
            'type': 'GET',
            data: { ToProcess: ToProcess },
            'access_token': User.access_token
        });
    },
    //Nuevo Presupuesto
    async Budgets(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Budgets',
            'type': 'POST',
            data: data,
            'access_token': User.access_token
        });
    },
    //Archivo a nuevo presupuesto
    //api/Budgets/File
    async FileBudget(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Budgets/File',
            'type': 'POST',
            'access_token': User.access_token,
            data: data,
        });
    },
    //Prespuestos
    //[Type] == Date/Top/Budget
    //[StartDate]
    //[EndDate]
    //[Employee]
    //[BudgetId]
    async GetBudgets(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Budgets',
            'type': 'GET',
            data: data,
            'access_token': User.access_token
        });
    },
    //SetUpdateBudgetDetail
    async SetUpdateBudgetDetail(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Budgets/' + data.BudgetId + '/Details/' + data.BudgetDetailId + '/Update',
            'type': 'POST',
            data: data,
            'access_token': User.access_token
        });
    },
    //SetChangeStatus
    //[BudgetId]
    //[Status] == Rechazado/Cancelado/Autorizado
    //[Comments]
    async SetChangeStatus(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Budgets/Authorize',
            'type': 'POST',
            data: data,
            'access_token': User.access_token
        });
    },
    //CheckAmountPolicy
    //[CatalogId]
    //[SubCatalogId]
    //[EmployeeId]
    //[State]
    //[StartDate]
    //[EndDate]
    async CheckAmountPolicy(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/Catalogs/' + data.CatalogId + '/SubCatalogs/' + data.SubCatalogId,
            'type': 'GET',
            data: data,
            'access_token': User.access_token
        });
    },
    //api/Account/{EmployeeId}/Transactions
    //[EmployeeId]
    //[Type]
    //[StartDate]
    //[EndDate]
    async Transactions(data) {
        let User = await this.GetUser();
        var _default = {
            EmployeeId: 0,
            Type: "Create",
            StartDate: 0,
            EndDate: 0,
            Limit: 10
        };

        var dataXHR = $.extend(_default, data);

        return Xhr.ajax({
            'path': '/api/Account/' + data.EmployeeId + '/Transactions/History',
            'type': 'GET',
            data: dataXHR,
            'access_token': User.access_token
        });
    },
    //api/Account/PaymentMethods
    async PaymentMethods() {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/PaymentMethods',
            'type': 'GET',
            'access_token': User.access_token
        });
    },
    //Presupuestos por empleado
    async GetBudgetsByEmployee(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Budgets',
            'type': 'GET',
            'access_token': User.access_token,
            'data': data
        });
    },
    //Presupuestos por transaccion    
    async GetBudgetsByTransaction(TransactionId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Budgets',
            'type': 'GET',
            'access_token': User.access_token,
            'data': {
                TransactionId: TransactionId
            }
        });
    },
    //SetTransactionManual
    async SetTransactionManual(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/Transaction/Manual',
            'type': 'POST',
            'access_token': User.access_token,
            'data': data
        });
    },
    //SetDeleteTransaction[193]
    //api/Expenses/Ticket/Manual/{TransactionManualId}/Delete
    //[TransactionManualId]
    async SetDeleteTransaction(TransactionManualId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/Ticket/Manual/' + TransactionManualId + '/Delete',
            'type': 'POST',
            'access_token': User.access_token
        });
    },
    //SetUploadManualTicket
    async SetUploadManualTicket(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/Ticket/Manual',
            'type': 'POST',
            'access_token': User.access_token,
            'data': data
        });
    },
    //GetDetailRelation
    //[RelationId]
    //[TransactionId]
    async GetDetailRelation(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/Details',
            'type': 'GET',
            'access_token': User.access_token,
            'data': data
        });
    },
    //SetUntieInvoice
    async SetUntieInvoice(RelationId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/Untie/' + RelationId,
            'type': 'POST',
            'access_token': User.access_token
        });
    },
    //Classificate
    async SetCatSubInvBalance(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/' + data.RelationId + '/Classificate',
            'type': 'POST',
            'access_token': User.access_token,
            'data': data
        });
    },
    //Facturas por transaccion
    async GetInvoicebyTransaction(TransactionId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/Invoices/Transaction/' + TransactionId,
            'type': 'GET',
            'access_token': User.access_token
        });
    },
    //Facturas por empleado
    async GetInvoicebyEmployee(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/' + data.EmployeeId + '/Invoices',
            'type': 'GET',
            'access_token': User.access_token,
            'data': data
        });
    },
    //SetTieInvoice
    async SetTieInvoice(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/Tie',
            'type': 'POST',
            'access_token': User.access_token,
            'data': data
        });
    },
    //SetFileManualTicket
    async SetFileManualTicket(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/Ticket/Manual/File',
            'type': 'POST',
            'access_token': User.access_token,
            'data': data
        });
    },
    //SetFileTransaction
    //api/Expenses/Transaction/Manual/File
    async SetFileTransaction(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/Transaction/Manual/File',
            'type': 'POST',
            'access_token': User.access_token,
            'data': data
        });
    },
    //SetFileManualTicket
    //api/Expenses/Ticket/Manual/File [175]
    async SetFileManualTicket(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/Ticket/Manual/File',
            'type': 'POST',
            'access_token': User.access_token,
            'data': data
        });
    },
    //GetCostCenterBySubcatalog
    async GetCostCenterBySubcatalog(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Account/' + data.EmployeeId + '/SubCatalogs/' + data.SubCatalogId + '/CostCenters',
            'type': 'GET',
            'access_token': User.access_token,
            'data': data
        });
    },
    //Notificacion
    //History/Unread
    async Notifications(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Notifications/App',
            'type': 'GET',
            'access_token': User.access_token,
            'data': data,
            'background': data.background
        });
    },
    //SetPopUpRead
    //api/Notifications/App/{NotificationId}
    async SetPopUpRead(NotificationId) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Notifications/App/' + NotificationId,
            'type': 'POST',
            'access_token': User.access_token,
            'data': { NotificationId: NotificationId }
        });
    },
    //SetFileMultiTicket
    //[InvoiceId]
    //[TransactionId]
    //[EmployeeId]
    async SetFileMultiTicket(data) {
        let User = await this.GetUser();

        return Xhr.ajax({
            'path': '/api/Expenses/Assistant/File',
            'type': 'POST',
            'access_token': User.access_token,
            'data': data
        });
    },
}