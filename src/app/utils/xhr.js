'use strict';
import $ from 'jquery';
import Constant from './constant';
import Router from '../router';

export default {
    ajax(params) {
        var that = this;

        var defaults = {
            "async": true,
            "crossDomain": true,
            url: Constant.SERVER_URL + params.path,
            type: params.type || 'GET',
            contentType: 'application/json',
            dataType: 'json',
            /*timeout: 10000,*/
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "bearer " + params.access_token,
                "Cache-Control": "no-cache",
            },
            beforeSend: function() {
                if (!params.background) {
                    app.loadingXHR(true);
                }
            }
        };

        $.extend(params, defaults);

        try {
            if (window.Connection) {
                return $.ajax(params)
                    .fail(function(error, textstatus, message) {
                        if (textstatus === "timeout") {
                            alert("Se agoto el tiempo de espera.Por favor vuelve a intentarlo");
                            app.loadingXHR(false);
                        } else {
                            switch (error.status) {
                                case 400:
                                    let mensajeError = that.getErrorMessage(error);
                                    app.loadingXHR(false);
                                    alert(mensajeError);
                                    break;
                                case 401:
                                    alert("La sesión ha expirado.");
                                    Router.View('login', true);
                                    break;
                                default:
                                    break;
                            }
                        }

                    })
                    .always(function() {
                        app.loadingXHR(false);
                    });
            } else {
                alert("comprueba tu conexion y vuelve a intentarlo");
                app.loadingXHR(false);
            }
        } catch (e) {
            alert(e);
            app.loadingXHR(false);
        }
    },
    getErrorMessage(err) {
        try {
            var responseJSON = err.responseJSON;
            var mensaje = responseJSON.Message;

            return mensaje;
        } catch (e) {
            return "Error de Comunicaciones.";
        }
    }
};