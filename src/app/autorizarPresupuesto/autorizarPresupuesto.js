/**
 *
 * Autorizar Presupuesto
 *
 */
import "./autorizarPresupuesto.less";
import templateHtml from "./autorizarPresupuesto.tpl.html";
import $ from 'jquery';
import jqueryValidation from 'jquery-validation';
import Jets from 'jets';
import moment from 'moment';
import Enumerable from 'linq';
import localforage from 'localforage';
import Tool from '../utils/tool';
import Store from '../store';


export default {
    async init() {
        //valida que el usuario tenga acceso...
        var Pass = await Store.Pass("Authorizer");
        var HasPermit = Pass.Data.HasPermit;

        if (HasPermit === "0") {
            alert("El usuario no tiene permisos para autorizar presupuestos.\nPonte en contacto con el administrador.");
            app.View("main");
            return;
        }
        this.render();
    },
    async render(data) {
        var fecha = moment().format("YYYY-MM-DD");
        var renderBudgets = [];
        if (!data) {
            var Budgets = await Store.GetBudgets({
                Type: "Top"
            });

            renderBudgets = Budgets.Data;
        } else {
            renderBudgets = data;
        }

        renderBudgets = Enumerable.from(renderBudgets).select(function(el) {
            el.showButtons = el.Status.Id === 6;
            return el;
        }).toArray();

        var renderTpl = Tool.renderTpl(templateHtml, {
            Presupuestos: renderBudgets,
            fecha: fecha
        });

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    async handleEvents() {
        var _this = this;
        $.validator.addMethod("greaterThan",
            function(value, element, params) {
                var fechaInicio = $(params).val();
                var fechaFin = moment(value, "YYYY-MM-DD");

                fechaInicio = moment(fechaInicio, "YYYY-MM-DD");

                return fechaFin.diff(fechaInicio) >= 0;
            }, 'fecha debe ser mayor que la fecha de inicio.');
        //Buscador de presupuestos
        $("style").remove();
        var jets = new Jets({
            searchTag: '#jetsSearch',
            contentTag: '#accordion'
        });
        //Buscar presupuestos por fecha
        $("#frm-Budgets").on("submit", function() {
            var $frm = $(this);
            if ($frm.valid()) {
                var data = $frm.serializeObject();
                console.log(data);

                Store.GetBudgets(data).then(function(Budgets) {
                    _this.render(Budgets.Data);
                });
            }

            return false;
        }).validate({
            'rules': {
                'StartDate': {
                    required: true
                },
                'EndDate': {
                    required: true,
                    greaterThan: "#StartDate"
                }
            },
            'messages': {
                'StartDate': {
                    required: 'Ingresa una fecha de inicio.'
                },
                'EndDate': {
                    required: 'Ingresa una fecha de fin.'
                }
            }
        });
        //edita el detalle de un presupuesto
        $(".btnSetUpdateBudgetDetail").on("click", function() {
            var $btn = $(this);
            var $parent = $btn.parent();
            var $input = $parent.find(".updateBudgetDetail");

            var data = {
                BudgetId: $input.data("budget"),
                BudgetDetailId: $input.data("detail"),
                Amount: $input.val(),
            };

            console.log(data);
            Store.SetUpdateBudgetDetail(data).then(function(r) {
                console.log(r);
                alert("Listo");
            });
        });
        //cambia de estatus un proyecto
        $(".btnSetChangeStatus").on("click", function() {
            var $btn = $(this);
            var status = $btn.data("status");
            var budget = $btn.data("budget");
            var Comments = $("#collapse-" + budget).find("textarea").val();

            if (status === "Autorizado") {
                Comments = "Autorizado";
            } else {
                if (!Comments) {
                    alert("Ingresa un comentario.");
                    return false;
                }
            }

            if (Comments) {
                Store.SetChangeStatus({
                    BudgetId: budget,
                    Status: status,
                    Comments: Comments,
                }).then(function(r) {
                    if (r.Success) {
                        alert(r.Data);
                        app.View("autorizarPresupuesto");
                    }
                });
            }

        });
    }
}