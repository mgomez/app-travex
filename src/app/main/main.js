/**
 *
 * Login
 *
 */
import "./main.less";
import $ from 'jquery';
import Tool from '../utils/tool';
import templateHtml from "./main.tpl.html";

export default {
    init(User) {
        this.render(User);
    },
    async render(User) {
        var renderTpl = Tool.renderTpl(templateHtml, User);

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    handleEvents() {}
}