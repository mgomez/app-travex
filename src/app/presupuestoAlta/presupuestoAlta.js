/**
 *
 * PRESUPUESTO - ALTA
 *
 */
import "./presupuestoAlta.less";
import templateHtml from "./presupuestoAlta.tpl.html";
//Parciales
import _presupuestoAlta_datos from "./_presupuestoAlta_datos.tpl.html";
import _presupuestoAlta_resumen from "./_presupuestoAlta_resumen.tpl.html";
import _resumenConceptos from "./_resumenConceptos.tpl.html";
import _empleados from "../shared/_empleados/_empleados";
import _nuevoConcepto from './_nuevoConcepto.tpl.html';
import optionsTpl from "../shared/options.tpl.html";

import $ from 'jquery';
import localforage from 'localforage';
import moment from 'moment';
import Enumerable from 'linq';
import Tool from '../utils/tool';
import Store from '../store';

export default {
    Data: {},
    async init() {
        //valida que el usuario tenga acceso...
        var Pass = await Store.Pass("Budget");
        var HasPermit = Pass.Data.HasPermit;

        if (HasPermit === "0") {
            alert("El usuario no tiene permisos para solicitar presupuestos.\nPonte en contacto con el administrador.");
            app.View("main");
            return;
        }
        this.render();
    },
    async render() {
        this.Data = {};
        this.Data.Details = [];
        var empleados = await _empleados.render("Budget");

        var renderTpl = Tool.renderTpl(templateHtml, {
            Partial: empleados
        });

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    async handleEvents() {
        var _this = this;

        _empleados.handleEvents();

        $(".empleado").on("click", async function() {
            var EmployeeId = $(this).data("id");
            var Employee = $(this).data("name");


            _this.Data.Employee = {
                EmployeeId: EmployeeId,
                FullName: Employee
            };

            console.log("EMPLEADO", _this.Data);

            //cargo los datos de los combos...
            var Proyectos = await Store.Projects(EmployeeId);
            var Estados = await Store.States();

            var renderTpl = Tool.renderTpl(_presupuestoAlta_datos, {
                Proyectos: Proyectos.Data,
                Estados: Estados.Data
            });

            $("#presupuestoAltaPartial").html(renderTpl);
            //seleccion de proyecto...
            $("#cbProyecto").on("change", function() {
                var Id = $(this).val();
                var Name = $(this).find("option:selected").text();

                _this.Data.Project = {
                    Id: Id,
                    Name: Name
                };
                console.log("PROYECTO", _this.Data);
            });
            //Seleccion de estado...
            $("#cbEstado").on("change", async function() {
                var StateId = $(this).val();
                var Name = $(this).find("option:selected").text();
                var Municipios = await Store.Municipalities(StateId);

                _this.Data.State = {
                    Id: StateId,
                    Name: Name
                };
                //valida que sea estado Nacional, si es asi, establece en NA el municipio
                if (StateId === "2") {
                    _this.Data.Municipality = {
                        Id: 1,
                        Name: "N/A"
                    };
                }
                console.log("ESTADO", _this.Data);
                //formato para template de options
                Municipios = Enumerable.from(Municipios.Data).select(function(el) {
                    return {
                        Value: el.Id,
                        Text: el.Name
                    }
                }).toArray();
                //render del template
                var options = Tool.renderTpl(optionsTpl, Municipios);
                //cargo en combo de municipios
                $("#cbMunicipio").html(options);
            });
            //seleccion de municipio...            
            $("#cbMunicipio").on("change", function() {
                var Id = $(this).val();
                var Name = $(this).find("option:selected").text();

                _this.Data.Municipality = {
                    Id: Id,
                    Name: Name
                };
                console.log("MUNICIPIO", _this.Data);
            });

            $(".btnSiguiente").on("click", async function() {
                if (!_this.Data.State) {
                    alert("Seleccione un estado para continuar.");
                    $("#cbEstado").focus();
                } else {
                    //sin el municipio es diferente a N/A valida que halla seleccionado un municipio
                    if (_this.Data.State.Id !== 2 && !_this.Data.Municipality) {
                        alert("Selecciona un municipio para continuar.");
                        return false;
                    }
                    var Conceptos = await Store.Catalogs(_this.Data.Employee.EmployeeId);

                    _this.Data.Catalogs = Enumerable.from(Conceptos.Data).select(function(el) {
                        return {
                            Value: el.Id,
                            Text: el.Name
                        }
                    }).toArray();
                    //fechas periodo
                    var periodoDefault = moment().format("YYYY-MM-DD");

                    _this.Data.periodoDefault = periodoDefault;
                    _this.Data.StartDate = periodoDefault;
                    _this.Data.EndDate = periodoDefault;

                    var template = Tool.renderTpl(_presupuestoAlta_resumen, _this.Data);

                    $("#presupuestoAltaPartial").html(template);
                    _this.renderPartialResumen();
                }
            });
        });

        _empleados.empleadoDefault();
    },
    async renderPartialResumen() {
        var _this = this;
        var collapseId = this.Data.Details.length + 1;

        _this.Data.collapseId = collapseId;

        var nuevoConceptoInit = Tool.renderTpl(_nuevoConcepto, _this.Data);
        $("#Details-container").append(nuevoConceptoInit);
        _this.handleEventsNuevoConcepto();

        //Archivo presupuesto
        $(".files64-button").on("click", function() {
            var $btn = $(this);
            var $container = $btn.parent(".files64");
            var pictureSource = $btn.data("source");

            app.getPicture(pictureSource).then(function(imageData) {
                console.log(imageData);
                $container.data("base", imageData);
                $container.find(".files64-prev-img").attr("src", "data:image/jpeg;base64," + imageData);
            }, function(err) {
                console.log(err);
            });

            _this.Data.File = true;
        });

        //Guarda las periodo en objeto global
        $(".datepicker").on("change", function() {
            var $this = $(this);
            var name = $this.prop("name");

            _this.Data[name] = $this.val();
        });

        //Nuevo Concepto
        $("#btnNuevoConcepto").on("click", function() {
            var isValid = true;
            //valida que no exista un concepto incompleto...
            $(".accordion").each(function(index, el) {
                var $frm = $(el).find(".Details");
                var data = $frm.serializeObject();

                var valido = _this.validarConcepto(data, true);

                if (!valido.Success) {
                    alert("Valide la información\n" + valido.Mensaje);
                    isValid = false;
                }
            });
            if (isValid) {
                //Agrrega template de nuevo concepto...
                _this.Data.collapseId = _this.Data.collapseId + 1;
                var template = Tool.renderTpl(_nuevoConcepto, _this.Data);
                //colapsa todos los conceptos
                $('.collapse').removeClass('show');

                $("#Details-container").append(template);
                _this.handleEventsNuevoConcepto();
            }

        });

        //Guardar presupesto en base
        $("#btnGuardarPrespuesto").on("click", function(e) {
            var $btn = $(this);
            var formData = $("#frm-budget").serializeObject();

            if (!formData.Description) {
                alert("Ingresa un objetivo.");
                $("#iDescription").focus();
                return false;
            }

            _this.Data.Details = [];
            var valid = true;

            $(".accordion").each(function(index, el) {
                var $frm = $(el).find(".Details");
                var data = $frm.serializeObject();

                var valido = _this.validarConcepto(data);

                if (!valido.Success) {
                    alert("Valide la información\n\n" + valido.Mensaje);
                    valid = false;
                } else {
                    _this.Data.Details.push({
                        "Catalog": {
                            "Id": data.CatalogId
                        },
                        "Subcatalog": {
                            "Id": data.SubcatalogId
                        },
                        "Amount": data.Amount,
                        "Description": data.Description
                    });
                }
            });
            if (valid) {
                $.extend(_this.Data, formData);

                console.log("BUDGETS", _this.Data, _this.Data.Description);

                $btn.attr("disabled", true);

                Store.Budgets(_this.Data).then(function(r) {
                    console.log(r);
                    var BudgetId = r.Data.Id;

                    if (r.Success) {
                        if (!_this.Data.File) {
                            alert("Se creo correctamente el presupuesto.\nPRESPUESTO: " + BudgetId);
                            app.View("main");
                        } else {
                            var files64 = $("#files64").data("base");

                            var data = {
                                BudgetId: BudgetId,
                                File: files64
                            };

                            Store.FileBudget(data).then(function(result) {
                                console.log(result);
                                if (!result) {
                                    alert("Se creo correctamente el presupuesto.\nPRESPUESTO: " + BudgetId + "\nOcurrio un error al guardar el archivo.");
                                } else {
                                    alert("Se creo correctamente el presupuesto.\nPRESPUESTO: " + BudgetId);
                                }
                                app.View("main");
                            });
                        }
                    }
                    $btn.attr("disabled", true);
                });
            }
        });

    },
    handleEventsNuevoConcepto() {
        var _this = this;
        var $accordionActual = $("#accordion-" + _this.Data.collapseId);
        var $cbConceptos = $accordionActual.find(".cbConceptos");
        var $cbClasificacion = $accordionActual.find(".cbClasificacion");
        var $btnEliminarConcepto = $accordionActual.find(".btnEliminarConcepto");
        //cambio en el combo de concepto, carga clasificaciones y cambia el header
        $cbConceptos.on("change", async function() {
            var $this = $(this);
            var $parent = $this.parent().parent();
            var accordion = $parent.data("accordion");
            var $accordion = $(accordion);

            var CadalogId = $this.val();
            var vcCatalog = $this.find("option:selected").text();

            $this.next("input[name=vcCatalog]").val(vcCatalog);

            _this.Data.CadalogId = CadalogId;

            $accordion.find(".accordion-header span").html(vcCatalog);

            $accordionActual.find(".AmountPolicy").val(0);

            var Clasificaciones = await Store.SubCatalogs(CadalogId);

            var renderOptions = Enumerable.from(Clasificaciones.Data).select(function(el) {
                return {
                    Value: el.Id,
                    Text: el.Name
                }
            }).toArray();

            var template = Tool.renderTpl(optionsTpl, renderOptions);

            $cbClasificacion.html(template);
        });
        //cambio en combo de clasificacion, carga el monto por politica
        $cbClasificacion.on("change", async function() {
            var $cb = $(this);
            var SubCatalogId = $cb.val();
            var vcSubcatalog = $cb.find("option:selected").text();

            $cb.next("input[name=vcSubcatalog]").val(vcSubcatalog);

            console.log("cbClasificacion", _this.Data);

            var data = {
                'CatalogId': _this.Data.CadalogId,
                'SubCatalogId': SubCatalogId,
                'EmployeeId': _this.Data.Employee.EmployeeId,
                'State': _this.Data.State.Id,
                'StartDate': _this.Data.StartDate,
                'EndDate': _this.Data.EndDate
            };
            Store.CheckAmountPolicy(data).then(function(r) {
                console.log("CheckAmountPolicy", r);
                if (r.Success) {
                    var AmountPolicy = r.Data.Amount;

                    $accordionActual.find(".AmountPolicy").val(AmountPolicy);
                }
            });
        });
        //Elimina concepto...
        $btnEliminarConcepto.on("click", function() {
            var $btn = $(this);
            var $frm = $btn.closest('.Detail');
            var info = $frm.serializeObject();
            var id = $(this).data("accordion");
            var accordions = $(".accordion").length;

            if (accordions > 1) {
                $(id).remove();

                $('html, body').animate({
                    scrollTop: $("#Details-container").offset().top
                }, 100);
                //elimina del objeto general...
                _this.Data.Details = Enumerable.from(_this.Data.Details).where(function(el) {
                    return el.Catalog.Id === info.CatalogId && el.Subcatalog.Id === info.SubcatalogId;
                }).toArray();
            } else {
                alert("El presupuesto debe tener mínimo un concepto.");
            }
        });
        //Guardar Concepto, colapsa acordeon
        $accordionActual.find(".btnCollapse").on("click", function() {
            var $btn = $(this);
            var type = $btn.data("type");
            var $frm = $btn.closest('.Details');
            var data = $frm.serializeObject();

            var valido = _this.validarConcepto(data);

            if (!valido.Success) {
                alert(valido.Mensaje);
                return false;
            } else {
                _this.Data.Details.push({
                    "Catalog": {
                        "Id": data.CatalogId,
                        "vcCatalog": data.vcCatalog
                    },
                    "Subcatalog": {
                        "Id": data.SubcatalogId,
                        "vcSubcatalog": data.vcSubcatalog
                    },
                    "Amount": +data.Amount,
                    "Description": data.Description
                });
            }

            $accordionActual.find('.collapse').removeClass('show');
            $btn.remove();
            //Agrego al resumen
            var renderTpl = Tool.renderTpl(_resumenConceptos, {
                Details: _this.Data.Details,
                Total: Enumerable.from(_this.Data.Details).sum("$.Amount")
            });
            $("#resumen-container").html(renderTpl);
            return false;
        });
    },
    validarConcepto(info, validExist) {
        var _this = this;
        var result = {
            Success: true,
            Mensaje: ""
        };

        if (!info.CatalogId) {
            result.Success = false;
            result.Mensaje = "✋ Selecciona un Concepto";
            return result;
        }
        if (!info.SubcatalogId) {
            result.Success = false;
            result.Mensaje = "✋ Selecciona una Clasificacion";
            return result;
        }

        if (!validExist) {
            var exist = Enumerable.from(_this.Data.Details).where(function(el) {
                return el.Catalog.Id === info.CatalogId && el.Subcatalog.Id === info.SubcatalogId;
            }).toArray();

            if (exist.length > 0) {
                result.Success = false;
                result.Mensaje = "✋ Ya existe un Concepto con esta clasificación.";
                return result;
            }
        }

        return result;
    }
}