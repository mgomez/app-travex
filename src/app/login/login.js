/**
 *
 * Login
 *
 */
import "./login.less";
import $ from 'jquery';
import jqueryValidation from 'jquery-validation';
import localforage from 'localforage';
import Tool from '../utils/tool';
import Router from '../router';
import Store from '../store';
import Constant from '../utils/constant';
import templateHtml from "./login.tpl.html";
import templateSuccessEmail from "./_successEmail.tpl.html";

function validLogin(formData) {
    $(".login-control").removeClass('invalid');

    if (!formData.userName) {
        $("#inputUser").addClass('invalid').focus();
        return false;
    }

    if (!formData.password) {
        $("#inputPassword").addClass('invalid').focus();
        return false;
    }

    return true;
}

export default {
    init() {
        this.render();
    },
    async render() {
        var UserTemp = await localforage.getItem("UserTemp");

        var renderTpl = Tool.renderTpl(templateHtml, { UserTemp: UserTemp });

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    handleEvents() {
        var _this = this;
        //Mostrar contraseña
        $("#mostrarContraseña").on("click", function() {
            var $el = $(this);

            $el.toggleClass('active');

            if ($el.hasClass('active')) {
                $("#inputPassword").attr("type", "text");
            } else {
                $("#inputPassword").attr("type", "password");
            }
        });
        //Acceder
        $("#frm-login").on("submit", function() {
            var formData = $(this).serializeObject();

            localforage.getItem("deviceId").then(function(deviceId) {
                try {
                    formData.deviceId = deviceId;
                    formData.systemDevice = device.platform;
                } catch (e) {}

                if (validLogin(formData)) {
                    app.loading(true);

                    _this.Login(formData).then(function(r) {
                        localforage.setItem('User', r);
                        localforage.setItem('UserTemp', formData.userName);

                        Router.View('main');
                        app.loading(false);
                        $(".navbar-name").html(r.userName);
                    });
                }
            });

            return false;
        });
        //Solicitar contraseña
        $("#frm-sendMailForgotPassword").on("submit", function() {
            var $frm = $(this);
            var $btnSubmit = $frm.find("button[type=submit]");

            if ($frm.valid()) {
                var data = $frm.serializeObject();

                $btnSubmit.attr("disabled", true);

                Store.ForgotPassword(data).then(function(r) {
                    $("#modal-olvideContra .modal-body").html(templateSuccessEmail);
                }, function() {
                    $btnSubmit.attr("disabled", false);
                });
            }
            return false;
        }).validate({
            'messages': {
                'Username': {
                    required: 'Ingresa un nombre de usuario.',
                }
            }
        });
    },
    Login(data) {
        return $.ajax({
                url: Constant.SERVER_URL + '/token',
                type: 'POST',
                dataType: 'json',
                data: data,
            })
            .fail(function(err) {
                if (err.status === 400) {
                    alert("Por favor revisa tu correo y contraseña. Los campos son sensibles a mayúsculas y minúsculas.");
                    app.loading(false);
                }
            });
    }
}