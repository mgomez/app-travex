/**
 *
 * SALDO
 *
 */
import "./saldo.less";
import templateHtml from "./saldo.tpl.html";
import "pretty-checkbox/dist/pretty-checkbox.min.css";
import $ from 'jquery';
import Enumerable from 'linq';
import localforage from 'localforage';
import Tool from '../utils/tool';
import Store from '../store';

function updateReloj(config) {
    if (config.time != 0) {
        config.$span.html(config.time + 's');
        config.time -= 0.01;
        config.time = config.time.toFixed(2);
        setTimeout(function() {
            updateReloj(config);
        }, 10);
    }
}

export default {
    init() {
        this.render();
    },
    async render() {
        var Tarjetas = await Store.Cards();

        Tarjetas = Enumerable.from(Tarjetas).select(function(el) {
            if (el.Transactions.length === 1 && el.Transactions[0].Amount === 0 && el.Transactions[0].Concept === "") {
                el.Transactions = null;
            }
            return el;
        }).toArray();
        console.log(Tarjetas);
        var renderTpl = Tool.renderTpl(templateHtml, {
            Tarjetas: Tarjetas
        });

        $("#renderBody").html(renderTpl);

        this.handleEvents();
    },
    handleEvents() {
        //asigna las imagenes de las tarjetas
        $(".tarjeta-img img").each(function(i, el) {
            var $img = $(el);
            var src = 'src/assets/img/' + $img.data("img");

            $img.attr("src", src);
        });
        //estatus al radio
        $(".ckSetAppCardONOFF").each(function(index, el) {
            var $ck = $(el);
            var status = $ck.data("status");
            var checked = status === 1;

            $ck.attr("checked", checked);
        });
        //muestra los moviemintos de la tarjeta
        $(".tarjeta-container").on("click", function() {
            var $tarjeta = $(this).parent();
            var $icono = $tarjeta.find(".tarjeta-container-toggleArrow");
            var $moviemintos = $tarjeta.find(".tarjeta-movimientos");

            $tarjeta.toggleClass('active');
            $icono.toggleClass('expand');
            $moviemintos.toggleClass('active');
        });

        //Bloquear tarjeta
        $(".ckSetAppCardONOFF").on("click", function() {
            var $ck = $(this);
            var vcCard = $ck.data("card");
            var status = $ck.data("status") === 1;
            var data = {
                vcCard: vcCard.substr(-4),
                vcProduct: "",
                pbActivate: status,
            };

            console.log("ckSetAppCardONOFF", data);

            Store.SetAppCardONOFF(data).then(function(r) {
                if (r.Success) {
                    var result = +r.Data.Value || false;
                    if (result) {
                        //valida para 0s
                        result = result === 0 ? 3 : result;
                        var timeout = (result + 6) * 1000;
                        $("#loadingSaldo-container").addClass('active');
                        updateReloj({
                            $span: $("#loadingSaldo-container span"),
                            time: result
                        });
                        setTimeout(function() {
                            $("#loadingSaldo-container").removeClass('active');
                            app.View("saldo");
                        }, timeout);
                    } else {
                        alert(r.Data.Value);
                    }
                }
            });
        });

    }
};